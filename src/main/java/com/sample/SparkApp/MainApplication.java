package com.sample.SparkApp;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalog.Catalog;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.spark.sql.execution.datasources.hbase.*;


/**
 * @author NARESHDhamija
 *
 */

public class MainApplication {

	 
	public MainApplication(JsonNode node) {
		// Read the Configurations from the JSON Node.
	}

	
	public static void main(String[] args) throws JsonProcessingException, IOException {
		SparkSession sparkSession = SparkSession.builder().appName("MainApplication").getOrCreate();
		
		// There will be seaprate Class to Read the configurations for Reading the Configuration specific to the Application.

		String jsonString = "{\r\n" + 
				"	\"App1\": {\r\n" + 
				"    	\"App1-conf\": {\r\n" + 
				"	\"Serialization\": \"\",\r\n" + 
				"	\"HBase-User\": \"root\",\r\n" + 
				"	\"HBase-Table\": \"Test\",\r\n" + 
				"	\"PathType\":\"HDFS\",\r\n" + 
				"	\"HDFSFILEPATH\":\"/tmp/SampleData\",\r\n" + 
				"	\"AddressDataFile\":\"/tmp/SampleData/address.csv\",\r\n" + 
				"	\"CustomersDataFile\":\"/tmp/SampleData/customers.csv\",\r\n" + 
				"	\"TransactionsDataFile\":\"/tmp/SampleData/transactions.csv\",\r\n" + 
				"	\"LOCALFILEPATH\":\"/home/xyz\"\r\n" + 
				"    	}\r\n" + 
				"	}\r\n" + 
				"}  ";
		
		// *	We are storing all the configurations in Node which will have details like 
		// *	HDFSFILEPATH
		// *	AddressDataFile
		// *    CustomersDataFile
		// *	TransactionsDataFile
		// * 	HBase tablename where the Records will be stored after Filtering/Processing.
		
		ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(jsonString);
        
        String AddressInputCSV = node.findPath("AddressDataFile").asText();
        String CustomerInputCSV = node.findPath("CustomersDataFile").asText();
        String TransactionsInputCSV = node.findPath("TransactionsDataFile").asText();
        
        Dataset<org.apache.spark.sql.Row> Customer;
        Dataset<org.apache.spark.sql.Row> Transactions;
        Dataset<org.apache.spark.sql.Row> Address;
        
        // If the Input is not a Parquet Format, read as Normal CSV
        
		if(!isParquetFormat(node))
		{	
			Transactions = sparkSession.read().format("csv").option("header","true").load(TransactionsInputCSV);
			Customer = sparkSession.read().format("csv").option("header","true").load(CustomerInputCSV);
			Address = sparkSession.read().format("csv").option("header","true").load(AddressInputCSV);
		}
		else // It is PARQUET
		{	
			Customer = sparkSession.read().option("header", true).parquet(CustomerInputCSV);
			Address = sparkSession.read().option("header", true).parquet(AddressInputCSV);
			Transactions = sparkSession.read().option("header", true).parquet(TransactionsInputCSV);
		}
        
        /*
         * https://stackoverflow.com/questions/43506662/spark-sql-joining-two-dataframes-datasets-with-same-column-name
         */
		
		ArrayList<String> customerAddressJoinCol = new ArrayList<String>();
		customerAddressJoinCol.add("AddressID");
        
        Dataset<org.apache.spark.sql.Row> customerAddressJoin = Customer.join(Address, scala.collection.JavaConversions.asScalaBuffer(customerAddressJoinCol), "inner").toDF("AddressID", "FirstName", "LastName", "AccountID", "Income", "City", "PostalCode");
        customerAddressJoin = customerAddressJoin.select("FirstName", "LastName", "AccountID", "Income", "City", "PostalCode");
                
        // Write the DataSET after joining the Customer Address into HDFS (Parquet Format)
        customerAddressJoin.write().mode(SaveMode.Overwrite).parquet("/tmp/SampleData/customerAddress.parquet1");
        
        ArrayList<String> transactionAddressJoinCol = new ArrayList<String>();
        transactionAddressJoinCol.add("AccountID");
        
        Dataset<org.apache.spark.sql.Row> transcationAddressJoin = Transactions.join(Address, scala.collection.JavaConversions.asScalaBuffer(customerAddressJoinCol), "inner");
        //transcationAddressJoin= transcationAddressJoin.selectExpr("AccountID","TransactionAmount","TransactionDate", "split(TransactionDate, '-')[0] as TransactionYear","split(TransactionDate, '-')[1] as TransactionMonth", "split(TransactionDate, '-')[2] as TransactionDay","City","PostalCode");
        transcationAddressJoin= transcationAddressJoin.selectExpr("AccountID","TransactionAmount","TransactionDate", "split(TransactionDate, '-')[0] as TransactionYear","split(TransactionDate, '-')[1] as TransactionMonth", "split(TransactionDate, '-')[2] as TransactionDay");
       
        // Write the DataSET after joining the Transaction and Address Dataset into HDFS (Parquet Format)
        transcationAddressJoin.write().mode(SaveMode.Overwrite).parquet("/tmp/SampleData/transactionAddress.parquet1");
        
        Dataset<org.apache.spark.sql.Row> FinalOutput = transcationAddressJoin.join(customerAddressJoin, scala.collection.JavaConversions.asScalaBuffer(transactionAddressJoinCol), "inner");
        FinalOutput.show();
        
        // Write the Final DataSET after joining the CustomerAddress and TransactionAddress into HDFS -- Parquet Format
        FinalOutput.write().mode(SaveMode.Overwrite).parquet("/tmp/SampleData/FinalOutput.parquet");
        
        FinalOutput.printSchema();
        // Write to the No SQL Database (HBase). For writing it Multiple times, change the Table Name Accordingly. 
        String catalog = "{" + 
        		"\"table\":{\"namespace\":\"default\", \"name\":\"nareshTable2\"}," + 
        		"\"rowkey\":\"AccountID\"," + 
        		"\"columns\":{" + 
        		"\"AccountID\":{\"cf\":\"rowkey\", \"col\":\"AccountID\", \"type\":\"string\"}," + 
        		"\"TransactionAmount\":{\"cf\":\"cf1\", \"col\":\"TransactionAmount\", \"type\":\"string\"}," +
        		"\"TransactionDate\":{\"cf\":\"cf1\", \"col\":\"TransactionDate\", \"type\":\"string\"}," +
        		"\"TransactionYear\":{\"cf\":\"cf1\", \"col\":\"TransactionYear\", \"type\":\"string\"}," + 
        		"\"TransactionMonth\":{\"cf\":\"cf1\", \"col\":\"TransactionMonth\", \"type\":\"string\"}," + 
        		"\"TransactionDay\":{\"cf\":\"cf1\", \"col\":\"TransactionDay\", \"type\":\"string\"}," + 
        		"\"City\":{\"cf\":\"cf1\", \"col\":\"City\", \"type\":\"string\"}," + 
        		"\"Postal\":{\"cf\":\"cf1\", \"col\":\"Postal\", \"type\":\"string\"}," + 
        		"\"FirstName\":{\"cf\":\"cf1\", \"col\":\"FirstName\", \"type\":\"string\"}," + 
        		"\"LastName\":{\"cf\":\"cf1\", \"col\":\"LastName\", \"type\":\"string\"}," + 
        		"\"Income\":{\"cf\":\"cf1\", \"col\":\"Income\", \"type\":\"string\"}," + 
        		"\"City\":{\"cf\":\"cf1\", \"col\":\"City\", \"type\":\"string\"}," + 
        		"\"PostalCode\":{\"cf\":\"cf1\", \"col\":\"PostalCode\", \"type\":\"string\"}" + 
        		"}" + 
        		"}".trim();

        // FinalOutput.printSchema();
        /*
         * 
        root
        |-- AccountID: string (nullable = true)
        |-- TransactionAmount: string (nullable = true)
        |-- TransactionDate: string (nullable = true)
        |-- TransactionYear: string (nullable = true)
        |-- TransactionMonth: string (nullable = true)
        |-- TransactionDay: string (nullable = true)
        |-- City: string (nullable = true)
        |-- Postal: string (nullable = true)
        |-- FirstName: string (nullable = true)
        |-- LastName: string (nullable = true)
        |-- Income: string (nullable = true)
        |-- City: string (nullable = true)
        |-- PostalCode: string (nullable = true)

         */

	    // Write the FinalOutput to No-SQL Database (HBase)
        Map<String, String> map = new HashMap<String, String>();
        map.put(HBaseTableCatalog.tableCatalog() ,catalog);
        // Details on which HBase to connect. 
        map.put(HBaseTableCatalog.newTable() ,"7");
        map.put("hbase.master", "node1.hdpcluster:16000");
        map.put("hbase.zookeeper.quorum", "node1.hdpcluster");
        map.put("zookeeper.znode.parent", "/hbase-unsecure");
        FinalOutput.write().options(map).format("org.apache.spark.sql.execution.datasources.hbase").save();
        
                
	}


	private static boolean isParquetFormat(JsonNode node) {
		String serializationType = node.findPath("Serialization").asText();
		if(serializationType.equalsIgnoreCase("Parquet"))
			return true;
		return false;
	}

}



