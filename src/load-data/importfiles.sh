#!/bin/bash

#Create a New Folder to put the Data in
hdfs dfs -mkdir /tmp/SampleData

#Copy the files
hdfs dfs -put /home/ndhamija/*.csv /tmp/SampleData

#Change the Permissions on the File
hdfs dfs -chmod -R 777 /tmp/SampleData